package com.example.mapdemo;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


public class ByCategoryRow extends BaseAdapter {
	Context context;

    private static LayoutInflater inflater = null;
    ArrayList<Instytucja> instytucje;

    public ByCategoryRow(Context context, ArrayList<Instytucja> opinie) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.instytucje = opinie;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return instytucje.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return instytucje.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.by_category_row, null);
        TextView ocena = (TextView) vi.findViewById(R.id.textView2);
        TextView opis = (TextView) vi.findViewById(R.id.textView1);
        TextView naglowek = (TextView) vi.findViewById(R.id.textView3);
        
        ocena.setText(String.valueOf(instytucje.get(position).getSrednia()));
        opis.setText(instytucje.get(position).getOpis());
        naglowek.setText(instytucje.get(position).getNazwa());
        return vi;
    }
}