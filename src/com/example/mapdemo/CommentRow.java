package com.example.mapdemo;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


public class CommentRow extends BaseAdapter {
	Context context;

    private static LayoutInflater inflater = null;
    ArrayList<Opinia> opinie;

    public CommentRow(Context context, ArrayList<Opinia> opinie) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.opinie = opinie;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return opinie.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return opinie.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.comment_row, null);
        TextView ocena = (TextView) vi.findViewById(R.id.textView2);
        TextView dataDodania = (TextView) vi.findViewById(R.id.textView1);
        TextView komentarz = (TextView) vi.findViewById(R.id.textView3);
        
        ocena.setText(String.valueOf(opinie.get(position).getOcena()));
        dataDodania.setText(opinie.get(position).getDate());
        komentarz.setText(opinie.get(position).getKomentarz());
        return vi;
    }
}