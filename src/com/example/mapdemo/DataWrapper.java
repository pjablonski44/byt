package com.example.mapdemo;

import java.io.Serializable;
import java.util.ArrayList;

public class DataWrapper implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	//private static final long serialVersionUID = 6573014487549092031L;
	private ArrayList<Instytucja> list;
	public DataWrapper(ArrayList<Instytucja> list) {
		this.list = list;
	}
	public ArrayList<Instytucja> getList() {
		return list;
	}
}
