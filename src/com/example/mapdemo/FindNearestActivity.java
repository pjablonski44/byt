package com.example.mapdemo;

import java.util.ArrayList;
import java.util.HashMap;

import android.location.Location;
import android.widget.AdapterView.OnItemClickListener;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class FindNearestActivity extends Activity {

	private ArrayList<Instytucja> instytucje;
	private ArrayList<Instytucja> najblizsze;
	private ListView listview;
	private Double latitude, longitude;
	private HashMap<Integer, Float> odleglosci;
	private EditText distance;
	private Button show;
	private float currentDistance;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_nearest);

		Intent intent = getIntent();
		DataWrapper dw1 = (DataWrapper) intent
				.getSerializableExtra("locations");
		instytucje = dw1.getList();
		latitude = intent.getDoubleExtra("latitude", 0);
		longitude = intent.getDoubleExtra("longitude", 0);
		distance = new EditText(this);
		distance = (EditText) findViewById(R.id.editText1);
		distance.setText("20");
		getWindow().setSoftInputMode(
      	      WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		show = new Button(this);
		show = (Button) findViewById(R.id.button1);
		listview = new ListView(this);
		listview = (ListView) findViewById(R.id.listView1);
		filterLocations((float)20);
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position,
					long id) {
				Intent myIntent = new Intent(FindNearestActivity.this,
						ViewDetailsActivity.class);
				int idInstytucji = instytucje.get(position).getId();
				myIntent.putExtra("id", String.valueOf(idInstytucji));
				FindNearestActivity.this.startActivity(myIntent);
			}

		});
		show.setOnClickListener(new OnClickListener() {
			String tmp=distance.getText().toString();
			@Override
			public void onClick(View v) {
				if(distance.getText().toString().equals("")) {
					filterLocations(1);
				} else {
					filterLocations(Float.valueOf(distance.getText().toString()));
				}
			}
		});
	}

	@SuppressLint("UseSparseArrays")
	private void filterLocations(float maxDistance) {
		najblizsze = new ArrayList<Instytucja>();
		odleglosci = new HashMap<Integer, Float>();
		for (Instytucja i : instytucje) {
			Location locationA = new Location("point A");

			locationA.setLatitude(latitude);
			locationA.setLongitude(longitude);

			Location locationB = new Location("point B");

			locationB.setLatitude(i.getLatitude());
			locationB.setLongitude(i.getLognitude());
			
			currentDistance = locationA.distanceTo(locationB);
			if (currentDistance <= maxDistance*1000) {
				najblizsze.add(i);
				odleglosci.put(i.getId(), currentDistance);
			}
		}
		listview.setAdapter(new NearestRow(this, najblizsze, odleglosci));
	}
}