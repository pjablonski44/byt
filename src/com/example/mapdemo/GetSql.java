package com.example.mapdemo;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.google.android.gms.maps.model.LatLng;

import com.google.android.gms.maps.model.Marker;

public class GetSql {
	
	private List<Marker> pMarkers = new ArrayList<Marker>();
	private Marker marker;
	private PreparedStatement ps = null;
	private ResultSet rs = null;
	private Connection connection = null;
	private LatLng position = null;
	private String name=null;
	
	public GetSql() {

		try {
			Class.forName("org.postgresql.Driver");
			//Thread.this.getContextClassLoader().loadClass("org.postgresql.Driver");
 
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return;
 
		}

		try {
			connection = DriverManager.getConnection(
					"jdbc:postgresql://pgsql-585316.vipserv.org/barcholt_lqt", "barcholt_pawel",
					"barcholt_pawel");
 
		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}
	}
	
	public List<Marker> GetAllMarkers() {
		
		try {
			ps = connection.prepareStatement("SELECT * FROM lokalizacje");
			rs = ps.executeQuery();
			
			while (rs.next()) {
				name = rs.getString("nazwa");
				position = new LatLng(rs.getDouble("lat"), rs.getDouble("lng"));
				marker.setTitle(name);
				marker.setPosition(position);
				pMarkers.add(marker);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return pMarkers;
	}
}
