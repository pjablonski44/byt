package com.example.mapdemo;

import java.io.Serializable;

public class Instytucja implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String nazwa;
	private Double latitude;
	private Double lognitude;
	private String imageName;
	private String opis;
	private boolean odwiedzona;
	private Integer id_kategorii;
	private Float srednia;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLognitude() {
		return lognitude;
	}
	public void setLognitude(Double lognitude) {
		this.lognitude = lognitude;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public boolean isOdwiedzona() {
		return odwiedzona;
	}
	public void setOdwiedzona(boolean odwiedzona) {
		this.odwiedzona = odwiedzona;
	}
	public Integer getId_kategorii() {
		return id_kategorii;
	}
	public void setId_kategorii(Integer id_kategorii) {
		this.id_kategorii = id_kategorii;
	}
	public Float getSrednia() {
		return srednia;
	}
	public void setSrednia(Float srednia) {
		this.srednia = srednia;
	}
	
	
}
