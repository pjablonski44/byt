package com.example.mapdemo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.zip.GZIPInputStream;



import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.os.StrictMode;
import android.util.Log;

public class JsonController {
	public static final String connectionString = "http://spolecznosciowy.zz.mu/test/";
	private static final String TAG = "HttpClient";
	ArrayList<Instytucja> listaInstytucji = new ArrayList<Instytucja>();
	
	
	public JsonController() {
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();

		StrictMode.setThreadPolicy(policy);
	}

	public ArrayList<Instytucja> getAll(String tmDevice) {

		String getAll = readStatement(connectionString + "getall.php?user=" + tmDevice);

		try {
			JSONArray jsonArray = new JSONArray(getAll);
			Log.i(JsonController.class.getName(), "Number of entries "
					+ jsonArray.length());
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				// Log.i(JsonController.class.getName(),
				// jsonObject.getString("text"));
				Instytucja instytucja = new Instytucja();
				instytucja.setId(jsonObject.getInt("id"));
				instytucja.setNazwa(jsonObject.getString("nazwa"));
				instytucja.setLatitude(jsonObject.getDouble("latitude"));
				instytucja.setLognitude(jsonObject.getDouble("lognitude"));
				instytucja.setOdwiedzona(jsonObject.getInt("odwiedzony")==1 ? true : false);
				//instytucja.setOdwiedzona(true);
				instytucja.setId_kategorii(jsonObject.getInt("id_kategorii"));
				//instytucja.setSrednia((float)5);
				instytucja.setSrednia((float)jsonObject.getDouble("srednia"));
				instytucja.setOpis(jsonObject.getString("opis"));
				instytucja.setImageName(jsonObject.getString("miniaturka"));
				listaInstytucji.add(instytucja);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return listaInstytucji;
	}

	public Instytucja getById(String id) {
		String getByPos = readStatement(connectionString + "getbypos.php?id="
				+ id);
		Instytucja inst = null;
		try {
			JSONArray jsonArray = new JSONArray(getByPos);

			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				// Log.i(JsonController.class.getName(),
				// jsonObject.getString("text"));
				inst = new Instytucja();
				inst.setId(jsonObject.getInt("id"));
				inst.setNazwa(jsonObject.getString("nazwa"));
				inst.setLatitude(jsonObject.getDouble("latitude"));
				inst.setLognitude(jsonObject.getDouble("lognitude"));
				inst.setImageName(jsonObject.getString("miniaturka"));
				inst.setOpis(jsonObject.getString("opis"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return inst;
	}

	private String readStatement(String address) {
		StringBuilder builder = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(address);
		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			} else {
				Log.e(JsonController.class.toString(),
						"Failed to download file");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return builder.toString();
	}

	public static JSONObject SendHttpPost(String URL, JSONObject jsonObjSend) {

		try {
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httpPostRequest = new HttpPost(URL);

			StringEntity se;
			se = new StringEntity(jsonObjSend.toString());

			// Set HTTP parameters
			httpPostRequest.setEntity(se);
			httpPostRequest.setHeader("Accept", "application/json");
			httpPostRequest.setHeader("Content-type", "application/json");
			httpPostRequest.setHeader("Accept-Encoding", "gzip"); // only set
																	// this
																	// parameter
																	// if you
																	// would
																	// like to
																	// use gzip
																	// compression

			long t = System.currentTimeMillis();
			HttpResponse response = (HttpResponse) httpclient
					.execute(httpPostRequest);
			Log.i(TAG,
					"HTTPResponse received in ["
							+ (System.currentTimeMillis() - t) + "ms]");

			// Get hold of the response entity (-> the data):
			HttpEntity entity = response.getEntity();

			if (entity != null) {
				// Read the content stream
				InputStream instream = entity.getContent();
				Header contentEncoding = response
						.getFirstHeader("Content-Encoding");
				if (contentEncoding != null
						&& contentEncoding.getValue().equalsIgnoreCase("gzip")) {
					instream = new GZIPInputStream(instream);
				}

				// convert content stream to a String
				String resultString = convertStreamToString(instream);
				instream.close();
				resultString = resultString.substring(1,
						resultString.length() - 1); // remove wrapping "[" and
													// "]"

				// Transform the String into a JSONObject
				JSONObject jsonObjRecv = new JSONObject(resultString);
				// Raw DEBUG output of our received JSON object:
				Log.i(TAG, "<JSONObject>\n" + jsonObjRecv.toString()
						+ "\n</JSONObject>");

				return jsonObjRecv;
			}

		} catch (Exception e) {
			// More about HTTP exception handling in another tutorial.
			// For now we just print the stack trace.
			e.printStackTrace();
		}
		return null;
	}

	private static String convertStreamToString(InputStream is) {
		/*
		 * To convert the InputStream to String we use the
		 * BufferedReader.readLine() method. We iterate until the BufferedReader
		 * return null which means there's no more data to read. Each line will
		 * appended to a StringBuilder and returned as String.
		 * 
		 * (c) public domain:
		 * http://senior.ceng.metu.edu.tr/2009/praeda/2009/01/
		 * 11/a-simple-restful-client-at-android/
		 */
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	public String ocenLokalizacje(Instytucja inst, float ocena, String tmDevice, String komentarz) {
		Integer ratedStatus=0;
		String result = "Co� posz�o nie tak";
		/*String checkUser = readStatement(connectionString + "isratedbyuser.php?id="
				+ inst.getId() + "&ocena=" + ocena + "&user=" + user);*/
		String checkUser = readStatement("http://partycypacja.org/lqt_ocena.php?id="
				+ inst.getId() + "&ocena=" + ocena + "&user=" + tmDevice + "&comment=" + komentarz);
		try {
			//JSONArray jsonArray = new JSONArray(checkUser);
			JSONObject jsonObject = new JSONObject(checkUser);

			//for (int i = 0; i < jsonArray.length(); i++) {
				//JSONObject jsonObject = jsonArray.getJSONObject(i);
				ratedStatus = jsonObject.getInt("rated");
			//}
		} catch (Exception e) {
			e.printStackTrace();
		}
		switch(ratedStatus) {
		case 1:
			return "Dzi�kujemy za ocen�!";
		case 2:
			return "Poprzednia ocena zosta�a zmieniona!";
		default:
			break;
		}
		return result;
	}

	public HashMap<String, Integer> getAllCategories() {
		HashMap<String,Integer> list = new HashMap<String,Integer>();
		String getCategories = readStatement(connectionString + "getallcategories.php");
		try {
			JSONArray jsonArray = new JSONArray(getCategories);

			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				list.put(jsonObject.getString("nazwa_kategorii"), jsonObject.getInt("id"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public ArrayList<Opinia> getComments(Integer id) {
		ArrayList<Opinia> result = new ArrayList<Opinia>();
		String getComments = readStatement(connectionString + "getComments.php?id=" + id);
		try {
			JSONArray jsonArray = new JSONArray(getComments);
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				Opinia opinia = new Opinia();
				opinia.setId(jsonObject.getInt("id"));
				opinia.setDate(jsonObject.getString("date"));
				opinia.setOcena((float)jsonObject.getDouble("ocena"));
				opinia.setUser(jsonObject.getString("user"));
				result.add(opinia);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
}
