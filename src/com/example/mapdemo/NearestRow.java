package com.example.mapdemo;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


public class NearestRow extends BaseAdapter {
	Context context;

    private static LayoutInflater inflater = null;
    ArrayList<Instytucja> instytucje;
    HashMap<Integer, Float> odleglosci;
    TextView ocena, opis, naglowek, distance;
    public NearestRow(Context context, ArrayList<Instytucja> opinie, HashMap<Integer, Float> odleglosci) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.instytucje = opinie;
        this.odleglosci = odleglosci;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return instytucje.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return instytucje.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        //if (vi == null)
            vi = inflater.inflate(R.layout.nearest_row, null);
        ocena = (TextView) vi.findViewById(R.id.textView2);
        opis = (TextView) vi.findViewById(R.id.textView1);
        naglowek = (TextView) vi.findViewById(R.id.textView3);
        distance = (TextView) vi.findViewById(R.id.textView4);
        
        String ocenaStr = String.valueOf(instytucje.get(position).getSrednia());
        String opisStr = instytucje.get(position).getOpis();
        String naglowekStr = instytucje.get(position).getNazwa();
        Float distancef = odleglosci.get(instytucje.get(position).getId());
        DecimalFormat df = new DecimalFormat("0.0");
        String distanceStr = String.valueOf(df.format(distancef)) + " km";
        
        ocena.setText(ocenaStr);
        opis.setText(opisStr);
        naglowek.setText(naglowekStr);
        distance.setText(distanceStr);
        
        return vi;
    }
}