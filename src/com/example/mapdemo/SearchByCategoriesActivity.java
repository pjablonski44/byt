package com.example.mapdemo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class SearchByCategoriesActivity extends Activity {
	JsonController json = new JsonController();
	ArrayList<Instytucja> locations, locationsInCategory;
	  @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
	@SuppressLint("NewApi")
	@Override
	  protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.search_by_categories);
	    
	    Intent intent = getIntent();
	    DataWrapper dw = (DataWrapper) intent.getSerializableExtra("data");
	    locations = dw.getList();
	    //locations = intent.getParcelableArrayListExtra("data");
	    
	    final ListView listview = (ListView) findViewById(R.id.listView1);
	    
	    final HashMap<String, Integer> map;
	    map = json.getAllCategories();
	    final ArrayList<String> list = new ArrayList<String>();
	    for(Entry<String, Integer> entry : map.entrySet()) {
	    	list.add(entry.getKey());
	    }
	    final StableArrayAdapter adapter = new StableArrayAdapter(this,
	        android.R.layout.simple_list_item_1, list);
	    listview.setAdapter(adapter);

	    listview.setOnItemClickListener(new OnItemClickListener() {

	      @TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@SuppressLint("NewApi")
		@Override
	      public void onItemClick(AdapterView<?> parent, final View view,
	          int position, long id) {
	        final String item = (String) parent.getItemAtPosition(position);
	        int clickedId = map.get(item);
	        locationsInCategory = new ArrayList<Instytucja>();
	        for(Instytucja i : locations) {
	        	if(i.getId_kategorii() == clickedId) {
	        		locationsInCategory.add(i);
	        	}
	        }
	        Intent viewByCategory = new Intent(SearchByCategoriesActivity.this, 
	        		ViewByCategoryActivity.class);
	        viewByCategory.putExtra("list", new DataWrapper(locationsInCategory));
	        SearchByCategoriesActivity.this.startActivity(viewByCategory);
	      }

	    });
	    
	  }

	  private class StableArrayAdapter extends ArrayAdapter<String> {

	    HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

	    public StableArrayAdapter(Context context, int textViewResourceId,
	        List<String> objects) {
	      super(context, textViewResourceId, objects);
	      for (int i = 0; i < objects.size(); ++i) {
	        mIdMap.put(objects.get(i), i);
	      }
	    }

	    @Override
	    public long getItemId(int position) {
	      String item = getItem(position);
	      return mIdMap.get(item);
	    }

	    @Override
	    public boolean hasStableIds() {
	      return true;
	    }

	  }

	} 