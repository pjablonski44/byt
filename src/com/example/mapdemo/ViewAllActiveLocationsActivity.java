/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.mapdemo;

import java.util.ArrayList;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import android.provider.Settings.System;

/**
 * This shows how to create a simple activity with a map and a marker on the
 * map.
 * <p>
 * Notice how we deal with the possibility that the Google Play services APK is
 * not installed/enabled/updated on a user's device.
 */
public class ViewAllActiveLocationsActivity extends FragmentActivity implements
		OnMyLocationButtonClickListener, ConnectionCallbacks,
		OnConnectionFailedListener, LocationListener {
	/**
	 * Note that this may be null if the Google Play services APK is not
	 * available.
	 */
	private int group1Id = 1;
	private String tmDevice;
	private int homeId = Menu.FIRST;
	private int allLocationsOption = Menu.FIRST + 1;
	private int visitedLocationsOption = Menu.FIRST + 2;
	private int unvisitedLocationsOption = Menu.FIRST + 3;
	private int findNearest = Menu.FIRST + 4;
	private int contactusId = Menu.FIRST + 5;
	private LocationClient mLocationClient;
	
	private static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(5000)         // 5 seconds
            .setFastestInterval(16)    // 16ms = 60fps
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
	
	private GoogleMap mMap;
	private ArrayList<Instytucja> listaInstytucji = new ArrayList<Instytucja>();

	public ViewAllActiveLocationsActivity() {

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		tmDevice = System.getString(this.getContentResolver(),
				System.ANDROID_ID);
		setContentView(R.layout.all_locations);

		setUpMapIfNeeded();
	}

	@Override
	protected void onResume() {
		super.onResume();
		setUpMapIfNeeded();
	}

	/**
	 * Sets up the map if it is possible to do so (i.e., the Google Play
	 * services APK is correctly installed) and the map has not already been
	 * instantiated.. This will ensure that we only ever call
	 * {@link #setUpMap()} once when {@link #mMap} is not null.
	 * <p>
	 * If it isn't installed {@link SupportMapFragment} (and
	 * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt
	 * for the user to install/update the Google Play services APK on their
	 * device.
	 * <p>
	 * A user can return to this FragmentActivity after following the prompt and
	 * correctly installing/updating/enabling the Google Play services. Since
	 * the FragmentActivity may not have been completely destroyed during this
	 * process (it is likely that it would only be stopped or paused),
	 * {@link #onCreate(Bundle)} may not be called again so we should call this
	 * method in {@link #onResume()} to guarantee that it will be called.
	 */
	private void setUpMapIfNeeded() {
		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (mMap == null) {
			// Try to obtain the map from the SupportMapFragment.
			mMap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();
			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				setUpMap();
			}
		}
		mMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
			@Override
			public void onInfoWindowClick(Marker marker) {
				// Intent intent = new
				// Intent(ViewAllActiveLocationsActivity.this,ViewDetailsActivity.class);
				// startActivity(intent);
				Intent myIntent = new Intent(
						ViewAllActiveLocationsActivity.this,
						ViewDetailsActivity.class);
				myIntent.putExtra("id", marker.getSnippet());
				ViewAllActiveLocationsActivity.this.startActivity(myIntent);

			}
		});
	}

	/**
	 * This is where we can add markers or lines, add listeners or move the
	 * camera. In this case, we just add a marker near Africa.
	 * <p>
	 * This should only be called once and when we are sure that {@link #mMap}
	 * is not null.
	 */
	private void setUpMap() {
		JsonController json = new JsonController();
		listaInstytucji = json.getAll(tmDevice);

		for (Instytucja i : listaInstytucji) {

			LatLng position = new LatLng(i.getLatitude(), i.getLognitude());
			mMap.addMarker(new MarkerOptions().position(position)
					.title(i.getNazwa()).snippet(i.getId().toString()));
		}
		mMap.setMyLocationEnabled(true);
		mMap.setOnMyLocationButtonClickListener(this);
		 mLocationClient = new LocationClient(
                 getApplicationContext(),
                 this,  // ConnectionCallbacks
                 this); // OnConnectionFailedListener
		// mMap.addMarker(new MarkerOptions().position(new LatLng(0,
		// 0)).title("default"));
		 mLocationClient.connect();
	}

	private void showAllLocations() {
		mMap.clear();

		for (Instytucja i : listaInstytucji) {

			LatLng position = new LatLng(i.getLatitude(), i.getLognitude());
			mMap.addMarker(new MarkerOptions().position(position)
					.title(i.getNazwa()).snippet(i.getId().toString()));
		}
	}

	private void showVisitedLocations() {
		mMap.clear();

		for (Instytucja i : listaInstytucji) {
			if (i.isOdwiedzona() == true) {
				LatLng position = new LatLng(i.getLatitude(), i.getLognitude());
				mMap.addMarker(new MarkerOptions().position(position)
						.title(i.getNazwa()).snippet(i.getId().toString()));
			}
		}
	}

	private void showUnvisitedLocations() {
		mMap.clear();

		for (Instytucja i : listaInstytucji) {
			if (i.isOdwiedzona() == false) {
				LatLng position = new LatLng(i.getLatitude(), i.getLognitude());
				mMap.addMarker(new MarkerOptions().position(position)
						.title(i.getNazwa()).snippet(i.getId().toString()));
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(group1Id, homeId, homeId, R.string.search_by_categories);
		menu.add(group1Id, allLocationsOption, allLocationsOption,
				R.string.show_all);
		menu.add(group1Id, visitedLocationsOption, visitedLocationsOption,
				R.string.show_visited);
		menu.add(group1Id, unvisitedLocationsOption, unvisitedLocationsOption,
				R.string.show_unvisited);
		menu.add(group1Id, findNearest, findNearest, R.string.find_nearest);
		menu.add(group1Id, contactusId, contactusId, "");

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case 1:
			// write your code here

			Toast msg = Toast.makeText(ViewAllActiveLocationsActivity.this,
					"Wyszukaj kategoriami", Toast.LENGTH_LONG);
			msg.show();
			Intent byCategoryIntent = new Intent(
					ViewAllActiveLocationsActivity.this,
					SearchByCategoriesActivity.class);
			byCategoryIntent.putExtra("data", new DataWrapper(listaInstytucji));
			// byCategoryIntent.putParcelableArrayListExtra("data", (ArrayList<?
			// extends Parcelable>) listaInstytucji);
			ViewAllActiveLocationsActivity.this.startActivity(byCategoryIntent);
			return true;

		case 2:
			Toast msgAll = Toast.makeText(ViewAllActiveLocationsActivity.this,
					"Wyświetlono wszystkie lokalizacje", Toast.LENGTH_LONG);
			msgAll.show();
			showAllLocations();
			return true;

		case 3:
			Toast msgVisited = Toast.makeText(
					ViewAllActiveLocationsActivity.this,
					"Wyświetlono odwiedzone lokalizacje", Toast.LENGTH_LONG);
			msgVisited.show();
			showVisitedLocations();
			return true;

		case 4:
			Toast msgUnvisited = Toast.makeText(
					ViewAllActiveLocationsActivity.this,
					"Wyświetlono nieodwiedzone lokalizacje", Toast.LENGTH_LONG);
			msgUnvisited.show();
			showUnvisitedLocations();
			return true;

		case 5:
			Toast msgNearest = Toast.makeText(
					ViewAllActiveLocationsActivity.this, "Wyszukaj w pobliżu",
					Toast.LENGTH_LONG);
			msgNearest.show();
			Intent nearestIntent = new Intent(
					ViewAllActiveLocationsActivity.this,
					FindNearestActivity.class);
			nearestIntent.putExtra("latitude", mLocationClient.getLastLocation().getLatitude());
			nearestIntent.putExtra("longitude", mLocationClient.getLastLocation().getLongitude());
			nearestIntent.putExtra("locations", new DataWrapper(listaInstytucji));
			// byCategoryIntent.putParcelableArrayListExtra("data", (ArrayList<?
			// extends Parcelable>) listaInstytucji);
			ViewAllActiveLocationsActivity.this.startActivity(nearestIntent);
			return true;

		case 6:
			// code here
			return true;

		default:
			break;

		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onMyLocationButtonClick() {
		Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT)
				.show();
		// Return false so that we don't consume the event and the default
		// behavior still occurs
		// (the camera animates to the user's current position).
		return false;
	}

	@Override
    public void onLocationChanged(Location location) {
    }

    /**
     * Callback called when connected to GCore. Implementation of {@link ConnectionCallbacks}.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        mLocationClient.requestLocationUpdates(
                REQUEST,
                this);  // LocationListener
    }

    /**
     * Callback called when disconnected from GCore. Implementation of {@link ConnectionCallbacks}.
     */
    @Override
    public void onDisconnected() {
        // Do nothing
    }

    /**
     * Implementation of {@link OnConnectionFailedListener}.
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Do nothing
    }
}
