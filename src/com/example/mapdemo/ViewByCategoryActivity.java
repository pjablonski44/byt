package com.example.mapdemo;

import java.util.ArrayList;
import android.widget.AdapterView.OnItemClickListener;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class ViewByCategoryActivity extends Activity {
	private JsonController json = new JsonController();
	private ArrayList<Instytucja> instytucje;
	private ListView listview;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		Intent intent = getIntent();
		DataWrapper dw1 = (DataWrapper) intent.getSerializableExtra("list");
		instytucje = dw1.getList();
		setContentView(R.layout.view_comments);
		
		
		listview = (ListView) findViewById(R.id.listView1);
		listview.setAdapter(new ByCategoryRow(this, instytucje));
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position,
					long id) {
				Intent myIntent = new Intent(
						ViewByCategoryActivity.this,
						ViewDetailsActivity.class);
				int idInstytucji = instytucje.get(position).getId();
				myIntent.putExtra("id", String.valueOf(idInstytucji));
				ViewByCategoryActivity.this.startActivity(myIntent);
			}

			
		});
	}
}