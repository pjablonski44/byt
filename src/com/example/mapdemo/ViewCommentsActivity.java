package com.example.mapdemo;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

public class ViewCommentsActivity extends Activity {
	JsonController json = new JsonController();
	ArrayList<Opinia> opinie = new ArrayList<Opinia>();
	ListView listview;
	TextView title;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Intent intent = getIntent();

		setContentView(R.layout.view_comments);
		String locationName = intent.getStringExtra("location");
		Integer locationId = intent.getIntExtra("id", 1);
		title = (TextView)findViewById(R.id.textView1);
		title.setText(locationName);
		super.onCreate(savedInstanceState);
		//opinie = json.getComments();
		
		/*Opinia opinia1 = new Opinia();
		opinia1.setDate("2013-12-12 14:33");
		opinia1.setId(1);
		opinia1.setKomentarz("superkomentarz");
		opinia1.setOcena(5);
		opinia1.setUser("d8dd83f");
		
		Opinia opinia2 = new Opinia();
		opinia2.setDate("2013-12-13 15:33");
		opinia2.setId(1);
		opinia2.setKomentarz("dupa dupa dupa");
		opinia2.setOcena((float) 3.5);
		opinia2.setUser("d83d83f");
		
		opinie.add(opinia1);
		opinie.add(opinia2);*/
		
		opinie = json.getComments(locationId);
		
		listview = (ListView) findViewById(R.id.listView1);
		listview.setAdapter(new CommentRow(this, opinie));
	}
}