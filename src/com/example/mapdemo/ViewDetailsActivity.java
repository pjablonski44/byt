package com.example.mapdemo;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import android.annotation.SuppressLint;
//import android.provider.Settings.Secure;
//import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
//import android.telephony.TelephonyManager;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import android.provider.Settings.System;

public class ViewDetailsActivity extends FragmentActivity {
	
	private TextView title, description;
	private EditText komentarz;
	private ImageView image;
	private JsonController json = new JsonController();
	private Instytucja inst;
	private URL url;
	private Bitmap bmp;
	private RatingBar ocenaGwiazdki;
	private float rating = 5;
	private Button ocen;
	private String tmDevice;
	private TextView viewComments;
	//String tmDevice = "a";
//	private final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
//
//   // private final String tmDevice, tmSerial, androidId;
//    tmDevice = "" + tm.getDeviceId();
	//private final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);

	
	//private String tmDevice;
    
	  @SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	@Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        Intent intent = getIntent();
	        String instId = intent.getStringExtra("id");
	        setContentView(R.layout.view_details);

			tmDevice= System.getString(this.getContentResolver(),System.ANDROID_ID);
	        title = new TextView(this);
	        title=(TextView)findViewById(R.id.textView1); 
	 		inst = json.getById(instId);
	 		title.setText(inst.getNazwa() + "; lat: " + inst.getLatitude()
	 				+ "; lon:" + inst.getLognitude());

	        description = new TextView(this);
	        description = (TextView)findViewById(R.id.textView2);
	        description.setText(inst.getOpis());
	        komentarz = new EditText(this);
	        komentarz = (EditText)findViewById(R.id.editText1);
	        
	        viewComments = (TextView)findViewById(R.id.TextView01);
	        
	        
	        getWindow().setSoftInputMode(
	        	      WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	 		image = new ImageView(this);
	 		image = (ImageView)findViewById(R.id.imageView1);
			try {
				url = new URL(JsonController.connectionString + "images/"+inst.getImageName());
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	 		
			try {
				bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	 		image.setImageBitmap(bmp);
	 		Display display = getWindowManager().getDefaultDisplay();
	 		int width = display.getWidth();  // deprecated
	 		int imageWidth = width - 2;
	 		int imageHeight = (int) (imageWidth * 0.75);
	 		image.getLayoutParams().width = imageWidth;
	 		image.getLayoutParams().height = imageHeight;
	 		
	 		ocenaGwiazdki = (RatingBar)findViewById(R.id.ratingBar1);
	 		ocenaGwiazdki.setRating(rating);
	 		ocen = (Button)findViewById(R.id.button1);
	 		ocen.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//tmDevice = "" + tm.getDeviceId();
					ocen.setText(json.ocenLokalizacje(inst, ocenaGwiazdki.getRating(), 
							tmDevice, komentarz.getText().toString()));
				}
			});
	 		viewComments.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent commentIntent = new Intent(
							ViewDetailsActivity.this,
							ViewCommentsActivity.class);
					commentIntent.putExtra("location", title.getText());
					commentIntent.putExtra("id", inst.getId());
					ViewDetailsActivity.this.startActivity(commentIntent);
				}
			});

	    }

}
